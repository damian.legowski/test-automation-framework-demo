package core.config_driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class DriverFactory {


    public static WebDriver startDriver(String desiredDriver) {
        WebDriver driver = null;
        if (desiredDriver.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver(CapabilitiesProvider.getChromeCapabilites());
        } else if (desiredDriver.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver(CapabilitiesProvider.getFirefoxCapabilities());
        } else {
            throw new WebDriverException(
                    desiredDriver + " is incorrect browser type. Please choose between: firefox, chrome");
        }
        return driver;
    }

}
