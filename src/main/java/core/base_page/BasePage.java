package core.base_page;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.json.JsonException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.List;
import java.util.function.Consumer;

public abstract class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor jsExecutor;
    private final int waitTimeout = 60;

    protected BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, waitTimeout);
        jsExecutor = ((JavascriptExecutor) driver);
    }

    protected WebElement waitForClickable(WebElement element) {
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected WebElement waitForVisibility(WebElement element) {
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected List<WebElement> waitForVisibilityOfAll(List<WebElement> elementsList) {
        return wait.until(ExpectedConditions.visibilityOfAllElements(elementsList));
    }

    protected WebElement clickOn(WebElement element) {
        try {
            element.click();
        } catch (Exception e) {
            waitForClickable(element).click();
        }
        return element;
    }

    protected String getTextOf(WebElement element) {
        String text = null;
        try {
            text = element.getText();
        } catch (Exception e) {
            text = waitForVisibility(element).getText();
        }
        return text;
    }

    protected String getAttribute(WebElement element, String attribute) {
        return waitForVisibility(element).getAttribute(attribute);
    }

    protected WebElement writeText(String text, WebElement inputElement) {
        try {
            inputElement.sendKeys(text);
        } catch (Exception e) {
            waitForVisibility(inputElement).sendKeys(text);
        }
        return inputElement;
    }

    protected boolean isDisplayedWithTimeout(WebElement element) {
        return isDisplayedWithTimeout(element, 10);
    }


    protected boolean isDisplayedWithTimeout(WebElement element, long timeOut) {
        long startTime = System.currentTimeMillis();
        while (!isTimeout(startTime, timeOut)) {
            try {
                if (element.isDisplayed()) {
                    return true;
                }
                sleep(500);
            } catch (NoSuchElementException | StaleElementReferenceException | JsonException e) {
                sleep(500);
            }
        }
        return false;
    }

    protected void sleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ignored) {
        }
    }

    private boolean isTimeout(long startTime, long desiredTimeoutInSeconds) {
        long start = startTime / 1000;
        return System.currentTimeMillis() / 1000 > start + desiredTimeoutInSeconds;
    }

    protected Select getSelect(WebElement element) {
        return new Select(waitForVisibility(element));
    }

    protected Actions getActions() {
        return new Actions(driver);
    }

    protected void moveToElement(WebElement webElement) {
        waitForVisibility(webElement);
        scrollIntoView(webElement);
        getActions().moveToElement(webElement).build().perform();
    }

    protected void moveToElementAndClick(WebElement webElement) {
        waitForVisibility(webElement);
        scrollIntoView(webElement);
        getActions().moveToElement(webElement)
                .click().
                build().perform();
    }

    protected WebElement clearAndWriteText(String text, WebElement inputElement) {
        waitForVisibility(inputElement);
        inputElement.clear();
        inputElement.sendKeys(text);
        return inputElement;
    }

    protected void scrollIntoView(WebElement element) {
        scrollIntoView(element, false);
    }

    protected void scrollIntoView(WebElement element, boolean alignToTop) {
        jsExecutor.executeScript("arguments[0].scrollIntoView(" + alignToTop + ");", element);
    }

    protected void performIfDisplayed(WebElement element, Consumer<WebElement> action, long timeout) {
        if (isDisplayedWithTimeout(element, timeout)) {
            action.accept(element);
        }
    }

    protected String getCurrentWindowHandle() {
        return driver.getWindowHandle();
    }

    protected void switchToDefaultFrame() {
        driver.switchTo().defaultContent();
    }

    protected String getCurrentFrameName() {
        JavascriptExecutor exe = (JavascriptExecutor) driver;
        return exe.executeScript("return self.name").toString();
    }

    protected void switchToFrame(String frameName) {
        driver.switchTo().frame(frameName);
    }

    protected void switchToFrame(WebElement element) {
        driver.switchTo().frame(element);
    }

    protected void switchToWindowHandle(String windowHandle) {
        driver.switchTo().window(windowHandle);
    }

    protected void takeScreenshot(String saveDir) {
        try {
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File directory = new File(saveDir);
            FileUtils.copyFile(screenshot, directory);
        } catch (Exception e) {
            System.out.println("Failed to take screenshot to path " + saveDir);
        }
    }
}
