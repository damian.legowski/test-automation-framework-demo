package core.context;

import java.util.HashMap;
import java.util.Map;

public class TestContext {

    private static Map<String, Object> testContext = new HashMap<>();

    public static synchronized void addToTestContext(String key, Object value) {
        long id = Thread.currentThread().getId();
        testContext.put(key + id, value);
    }

    public static synchronized Object getFromTestContext(String key) {
        return testContext.get(key+Thread.currentThread().getId());
    }

    public static synchronized void deleteTestContext(){
        testContext.clear();
    }
}
