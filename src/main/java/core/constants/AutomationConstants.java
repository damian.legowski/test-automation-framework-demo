package core.constants;

public class AutomationConstants {

    public static final String USER_DIR = System.getProperty("user.dir");
    public static final String TEST_RESOURCES_DIR = USER_DIR + "/src/test/resources/";
    public static final String MAIN_RESOURCES_DIR = USER_DIR + "/src/main/resources/";

    public static final String BROWSER = System.getProperty("browser", "chrome");
}
