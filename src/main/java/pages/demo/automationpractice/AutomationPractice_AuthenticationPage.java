package pages.demo.automationpractice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutomationPractice_AuthenticationPage extends AutomationPractice_TopMenuHeaderPage {

    @FindBy(xpath = "//input[@id='email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id='passwd']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[@id='SubmitLogin']")
    private WebElement signInButton;

    public AutomationPractice_AuthenticationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public AutomationPractice_AuthenticationPage fillEmailField(String email) {
        writeText(email, emailField);
        return this;
    }

    public AutomationPractice_AuthenticationPage fillPasswordField(String password) {
        writeText(password, passwordField);
        return this;
    }

    public AutomationPractice_AuthenticationPage clickOnSignInButtonAuthentication() {
        clickOn(signInButton);
        return this;
    }
}
