package pages.demo.automationpractice;

import core.base_page.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AutomationPractice_TopMenuHeaderPage extends BasePage {

    @FindBy(xpath = "//a[@class='login']")
    private WebElement signInButtonAtTopRightMenuBar;

    @FindBy(xpath = "//a[@class='account']")
    private WebElement accountIconAtMenuBar;

    public AutomationPractice_TopMenuHeaderPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public AutomationPractice_TopMenuHeaderPage clickOnSignInButtonAtTopRightMenuBar(){
        clickOn(signInButtonAtTopRightMenuBar);
        return this;
    }

    public boolean isMyAccountIconVisibleAtMenuBar(){
        return isDisplayedWithTimeout(accountIconAtMenuBar,6);
    }
}
