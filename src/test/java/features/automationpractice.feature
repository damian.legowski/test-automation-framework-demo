Feature: AutomationPractice Demo

  @demo
  Scenario: Customer login - successful
    Given I am at automationpractice home page
    When I click on Sign in button at top menu bar
    And I provide email: "test@testd.com" to Email address field
    And I provide password: "testd" to Password field
    And I click on Sign in button in authentication page
    Then I can see my logged user on menu bar