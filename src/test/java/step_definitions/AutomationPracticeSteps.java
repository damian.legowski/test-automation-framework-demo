package step_definitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.demo.automationpractice.AutomationPractice_AuthenticationPage;
import pages.demo.automationpractice.AutomationPractice_TopMenuHeaderPage;
import step_definitions.step_hooks.StepHooks;

import static org.assertj.core.api.Assertions.assertThat;

public class AutomationPracticeSteps {

    private static String AUTOMATION_PRACTICE_URL = "http://automationpractice.com/index.php";

    private WebDriver driver;

    private AutomationPractice_TopMenuHeaderPage topMenuHeaderPage;
    private AutomationPractice_AuthenticationPage authenticationPage;

    public AutomationPracticeSteps(StepHooks stepHooks) {
        this.driver = stepHooks.getDriver();
        topMenuHeaderPage = new AutomationPractice_TopMenuHeaderPage(driver);
        authenticationPage = new AutomationPractice_AuthenticationPage(driver);
    }

    @Given("I am at automationpractice home page")
    public void iOpenAutomationPracticeHomePage(){
        driver.get(AUTOMATION_PRACTICE_URL);
    }

    @When("I click on Sign in button at top menu bar")
    public void iClickOnSignInButton() {
        topMenuHeaderPage.clickOnSignInButtonAtTopRightMenuBar();
    }

    @And("I provide email: {string} to Email address field")
    public void iProvideEmailToEmailAddressField(String email) {
        authenticationPage.fillEmailField(email);
    }

    @And("I provide password: {string} to Password field")
    public void iProvidePasswordToPasswordField(String password) {
        authenticationPage.fillPasswordField(password);
    }

    @And("I click on Sign in button in authentication page")
    public void iClickOnSignInButtonInAuthenticationPage() {
        authenticationPage.clickOnSignInButtonAuthentication();
    }

    @Then("I can see my logged user on menu bar")
    public void iCanSeeMyLoggedUserOnMenuBar() {
        assertThat(authenticationPage.isMyAccountIconVisibleAtMenuBar())
                .isTrue();
    }
}
