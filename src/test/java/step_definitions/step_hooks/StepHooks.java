package step_definitions.step_hooks;

import core.config_driver.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;

import static core.constants.AutomationConstants.BROWSER;

public class StepHooks {

    private WebDriver driver;

    @Before()
    public void launch(Scenario scenario) {
        driver = DriverFactory.startDriver(BROWSER);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After()
    public void teardownSelenium(Scenario scenario) {
        if (driver != null) {
            if (scenario.isFailed()) {
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screenshot, "image/png","failure screenshot");
            }
            driver.quit();
        }
    }

    public WebDriver getDriver() {
        return driver;
    }
}
