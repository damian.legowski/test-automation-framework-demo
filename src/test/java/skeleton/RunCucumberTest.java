package skeleton;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/cucumber-report.json", "pretty", "html:target/cucumber-reports.html"},
        features = {"src/test/java/features" },
        glue = {"step_definitions"}
)
public class RunCucumberTest {

}
